@if (count($errors) > 0)
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<form method="post" action="{{ route('result') }}" enctype="multipart/form-data">
    @csrf
    <div class="image">
        <input type="file" required name="image">
    </div>
    <div class="button">
        <button type="submit">Send</button>
    </div>
</form>
