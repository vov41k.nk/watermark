<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    public function upload(){
        return view('upload');
    }

    public function result(Request $request){
        $this->validate($request, [
            'image' => 'required|image|mimes:jpg,jpeg,png|max:4096|dimensions:min_width=200,min_height=200',
        ]);

        $file = $request->file('image');

        $imageInfo = getimagesize($file->getRealPath());

        $imageWidth = (int)$imageInfo[0];
        $imageHeight = (int)$imageInfo[1];
        $imageMime = $imageInfo['mime'];

        if ($imageMime == 'image/png') {
			$image = imagecreatefrompng($file->getRealPath());
		} elseif ($imageMime == 'image/jpeg') {
			$image = imagecreatefromjpeg($file->getRealPath());
		} else {
            throw new \Exception('The image must be a file of type: jpg, jpeg, png.');
        }

        $r = $g = $b = 0;

        for ($i = 0; $i < $imageWidth; $i++) {
            for ($j = 0; $j < $imageHeight; $j++) {
                $color = imagecolorat($image, $i, $j);

                $r += ($color >> 16) & 0xFF;
                $g += ($color >> 8) & 0xFF;
                $b += $color & 0xFF;
            }
        }

        if ($r >= $g && $r >= $b) {
            $watermarkImage = imagecreatefrompng(public_path('watermarks/black.png'));
        } else if ($g >= $r && $g >= $b) {
            $watermarkImage = imagecreatefrompng(public_path('watermarks/yellow.png'));
        } else if ($b >= $r && $b >= $g) {
            $watermarkImage = imagecreatefrompng(public_path('watermarks/red.png'));
        } else {
            $watermarkImage = imagecreatefrompng(public_path('watermarks/white.png'));
        }

        $watermarkImageWidth = imagesx($watermarkImage);
        $watermarkImageHeight = imagesy($watermarkImage);

        imagecopy($image, $watermarkImage, ($imageWidth - $watermarkImageWidth) / 2, ($imageHeight - $watermarkImageHeight) / 2, 0, 0, $watermarkImageWidth, $watermarkImageHeight);

        if ($imageMime == 'image/png') {
            header("Content-type: image/png");
            imagepng($image);
		} elseif ($imageMime == 'image/jpeg') {
			header("Content-type: image/jpeg");
            imagejpeg($image);
		}

        imagedestroy($image);
        imagedestroy($watermarkImage);
    }
}
